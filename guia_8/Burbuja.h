#include <iostream>

using namespace std;

#ifndef BURBUJA_H
#define BURBUJA_H

// Clase Burbuja
class Burbuja{

    public:
        // Constructor
        Burbuja();
        // Método de ordenamiento por intercambio menor
        double burbuja_menor(int *array, int n);
        // Método de ordenamiento por intercambio mayor
        double burbuja_mayor(int *array, int n);
};
#endif
