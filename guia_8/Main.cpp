#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "Burbuja.h"
#include "Insercion.h"
#include "Seleccion.h"
#include "Quicksort.h"


using namespace std;

void imprimir_tiempos(double *tiempo, string *metodo){

    cout << endl;
    cout << "--------------------------------------------------" << endl;
    cout << "  Método             |  Tiempo" << endl;
    cout << "--------------------------------------------------" << endl;

    for(int i=0; i<7; i++){
        cout << metodo[i] << string(21-metodo[i].length(), ' ') << "|" << tiempo[i] << endl;
    }
    cout << "--------------------------------------------------" << endl;

}


void generar_y_ordenar(int n, string arg){

    int random;
    Burbuja burbuja;
    Insercion insercion;
    Seleccion seleccion;
    Quicksort quicksort;
    int vector[8][n];
    double tiempo[7];
    srand(time(0));


    for(int i=0; i<n; i++){
        random = rand()%n + 1;

        for(int j=0; j<8; j++){
            vector[j][i] = random;
        }
    }

    tiempo[0] = burbuja.burbuja_menor(vector[0], n);
    tiempo[1] = burbuja.burbuja_mayor(vector[1], n);
    tiempo[2] = insercion.ordenar_insercion(vector[2], n);
    tiempo[3] = insercion.ordenar_insercion_binaria(vector[3], n);
    tiempo[4] = insercion.ordenar_shell(vector[4], n);
    tiempo[5] = seleccion.ordenar_seleccion(vector[5], n);
    tiempo[6] = quicksort.quicksort(vector[6], n);


    string metodo[7] = {"Burbuja Menor", "Burbuja Mayor", "Insercion",
                        "Insercion Binaria", "Shell", "Seleccion", "Quicksort"};

    imprimir_tiempos(tiempo, metodo);

    if(arg == "s"){
        cout << "--------------------------------------------------" << endl;
        cout << "Vector original:" << endl << "|";

        for(int i=0; i<n; i++){
            cout << vector[7][i] << "|";
        }
        cout << endl;
        cout << "--------------------------------------------------" << endl;

        for(int i=0; i<7; i++){
            cout << metodo[i] << string(21-metodo[i].length(), ' ') << "|";

            for(int j=0; j<n; j++){
                cout << vector[i][j] << "|";
            }
            cout << endl;
        }

    }
}


int main(int argc, char **argv){

    int n;
    string arg;

    if (argc != 3){
        cout << "Debe ingresar 2 parámetros" << endl;
    }

    else{

        try{
            n = stoi(argv[1]);
            arg = argv[2];

            if(n > 1000000 || n < 1){
                cout << "El tamaño ingresado para el vector no es válido: ";
                cout << "debe ser un número positivo menor a 1.000.000" << endl;
                exit(1);
            }

            else if (arg != "n" && arg != "s"){
                cout << "Parámetro no válido, utilice 's' o 'n' como parámetro" << endl;
                exit(1);
            }
        }
        catch (const std::invalid_argument& e){
            cout << "Error: el primer parámetro debe ser un número" << endl;
            exit(1);
        }

        generar_y_ordenar(n, arg);
    }

    return 0;
}
