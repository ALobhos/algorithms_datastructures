#include <iostream>

using namespace std;

#ifndef INSERCION_H
#define INSERCION_H

class Insercion{

    public:
        Insercion();
        double ordenar_insercion(int *array, int n);
        double ordenar_insercion_binaria(int *array, int n);
        double ordenar_shell(int *array, int n);
};
#endif
