#include <iostream>
#include "ListaIngredientes.h"

using namespace std;

// Definición de la estructura "Nodo", que contiene un puntero del tipo nodo
// que sirve de enlace, y un atributo para datos, en este caso un string
typedef struct _Nodo{
    string nombre;
    ListaIngredientes *ingredientes;
    struct _Nodo *sig = NULL;
} Nodo;
