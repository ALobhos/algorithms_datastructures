#include <iostream>

using namespace std;

// Se define una estructura de nombre ingrediente, que básicamente es un nodo
// con el nombre del ingrediente y un puntero al siguiente ingrediente
typedef struct _Ingrediente{

    string nombre;
    struct _Ingrediente *sig = NULL;
}Ingrediente;
