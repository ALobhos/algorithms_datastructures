#include <iostream>
#include "ListaIngredientes.h"

using namespace std;


// Constructor por defecto
ListaIngredientes::ListaIngredientes() {}


// Función que permite añadir ingredientes sin importar el orden
void ListaIngredientes::add_ingrediente(string nombre){

    Ingrediente *temp = new Ingrediente;
    temp->nombre = nombre;

    // En caso de que no existan nodos en la lista, el primero y a la vez el
    // último va a ser el único nodo existente
    if (this->primero == NULL){
        this->primero = temp;
        this->ultimo = temp;
    }
    // Si no, simplemente se añaden al final de la lista
    else{
        this->ultimo->sig = temp;
        this->ultimo = temp;
    }

    cout << "Ingrediente añadido con éxito!" << endl;

}


// Función que permite la eliminación de un determinado ingrediente (si es que existe)
void ListaIngredientes::eliminar_ingrediente(int posicion){

    Ingrediente *aux = this->primero;
    Ingrediente *anterior = NULL;
    bool existe = 0;

    // Si el ingrediente seleccionado es el primero y la lista no esta vacía,
    // se elimina directamente
    if(posicion == 1 && aux != NULL){
        this->primero = aux->sig;
        aux->sig == NULL;
        existe = 1;
    }
    else{

        int posicion_actual = 1;

        // Si no es el primer elemento, se busca si existe la posición que se
        // desea eliminar
        while(aux != NULL){

            if(posicion_actual == posicion){
                anterior->sig = aux->sig;
                aux->sig = NULL;
                existe = 1;
                break;
            }

            anterior = aux;
            aux = aux->sig;
            posicion_actual++;
        }
    }

    // Se utiliza el booleano "existe" para informar al usuario si el
    // elemento fue eliminado o si simplemente no existe
    if(existe){bool existe = 0;
        cout << "Eliminación exitosa" << endl;
    }
    else{
        cout << "El elemento seleccionado no existe..." << endl;
    }
}


// Función que imprime la lista de ingredientes
void ListaIngredientes::imprimir_ingredientes(){

    // Se instancia una variable auxiliar para recorrer la lista sin perder
    // los valores de la lista
    Ingrediente *aux = this->primero;
    int i = 1;

    // Siempre que exista un nodo (el valor del actual no sea NULL), se irá
    // imprimiendo sus valores
    cout << endl;
    while (aux != NULL){
        cout << i << "- " << aux->nombre << endl;
        aux = aux->sig;
        i++;
    }
    cout << endl;
}
