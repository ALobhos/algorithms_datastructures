#include <iostream>
#include <stdlib.h>
#include "Lista.h"


using namespace std;


// Constructor por defecto de la clase "Lista"
Lista::Lista() {}


// Función que permite la creación y enlazado de nodos
void Lista::crear_ordenados(string nombre){

    Nodo *temp = new Nodo;
    temp->nombre = nombre;
    temp->ingredientes = new ListaIngredientes();

    // En caso de que no existan nodos en la lista, el primero y a la vez el
    // último va a ser el único nodo existente
    if (this->primero == NULL){
        this->primero = temp;
        this->ultimo = temp;
    }

    // En caso de que si exista al menos un elemento en la lista, se pasa a
    // comprobar la posición en que deberá ir el nodo
    else{

        // Se instancia una variable que permita recorrer la lista de nodos y
        // se guarda el nodo anterior, en caso de que empiecen dos palabras
        // con la misma letra pero una va antes que la otra
        Nodo *actual = this->primero;
        Nodo *anterior = NULL;

        // Mientras existan nodos en la lista, se comprobará la posición en
        // la que debe ir el nodo
        while(actual != NULL){

            // Si el valor en ascii de la primera letra es menor que la primera,
            // se posiciona al principio de la lista
            if ((int)temp->nombre[0] < (int)actual->nombre[0]){
                this->primero = temp;
                temp->sig = actual;
                break;
            }

            // Si el valor es mayor que el último de la lista, se coloca el
            // nodo al final de la lista y se enlaza como debe
            else if ((int)temp->nombre[0] > (int)this->ultimo->nombre[0]){
                this->ultimo->sig = temp;
                this->ultimo = temp;
                break;
            }

            // En caso de ser la misma letra, se pasa a revisar las siguientes
            else if ((int) temp->nombre[0] == (int) actual->nombre[0]){

                int i = 1;
                bool repetido = 1;

                // Si existe mas de una vez el mismo nombre, solo se compara el
                // último
                if (actual->sig != NULL){
                    if(actual->nombre == actual->sig->nombre){
                        actual = actual->sig;
                        continue;
                    }
                }

                // Se compara cada letra hasta ver cuál corresponde alfabeticamente
                while(temp->nombre[i] != '\0' || actual->nombre[i] != '\0'){

                    if((int)temp->nombre[i] > (int)actual->nombre[i]){
                        temp->sig = actual->sig;
                        actual->sig = temp;
                        repetido = 0;
                        break;
                    }

                    else if((int)temp->nombre[i] < (int)actual->nombre[i]){

                        if (anterior != NULL){
                            temp->sig = anterior->sig;
                            anterior->sig = temp;
                        }
                        else{
                            temp->sig = this->primero;
                            this->primero = temp;
                        }
                        repetido = 0;
                        break;
                    }
                    i++;
                }

                // Si todas las letras son iguales, se agrega el nodo al lado
                // de su repetido
                if(repetido){
                    temp->sig = actual->sig;
                    actual->sig = temp;
                }
                break;
            }

            // Si el valor es mayor o igual al elemento actual, o menor
            // o igual al elemento siguiente, se cambian los respectivos
            // punteros y se inserta el nodo
            else if ((int)temp->nombre[0] > (int)actual->nombre[0] && (int)temp->nombre[0] < (int)actual->sig->nombre[0]){
                temp->sig = actual->sig;
                actual->sig = temp;
                break;
            }

            // Si no se cumple ninguna de las anteriores, se pasa al siguiente
            // nodo de la lista para hacer comparaciones
            anterior = actual;
            actual = actual->sig;
        }
    }
}


// Función que permite la eliminación de un nodo específico
void Lista::eliminar_nodo(int posicion){

    Nodo *aux = this->primero;
    Nodo *anterior = NULL;

    // Si el nodo seleccionado es el primero (y la lista no está vacía), se
    // elimina el primer elemento de la lista
    if(posicion == 1 && aux != NULL){
        this->primero = aux->sig;
        aux->sig == NULL;
    }
    else{

        int posicion_actual = 1;
        bool existe = 0;

        // Mientras existan nodos, se comprobará si existe la posición en la
        // que se quiere eliminar un nodo
        while(aux != NULL){

            // Si la posicion existe, se elimina el nodo correspondiente
            if(posicion_actual == posicion){
                anterior->sig = aux->sig;
                aux->sig = NULL;
                existe = 1;
                break;
            }

            anterior = aux;
            aux = aux->sig;
            posicion_actual++;
        }

        // Se usa el booleano "existe" para informar al usuario si se eliminó
        // el elemento o simplemente no existe
        if(existe){
            cout << "Eliminación exitosa" << endl;
        }
        else{
            cout << "El elemento seleccionado no existe..." << endl;
        }
    }
}


// Función que imprime la lista
void Lista::imprimir(){

    // Se instancia una variable auxiliar para recorrer la lista sin perder
    // los valores de la lista
    Nodo *aux = this->primero;
    int i = 1;

    // Siempre que exista un nodo (el valor del actual no sea NULL), se irá
    // imprimiendo sus valores
    while (aux != NULL){
        cout << i << "- " << aux->nombre << endl;
        aux = aux->sig;
        i++;
    }
}


// Función que permite la selección de un nodo para revisar sus elementos
void Lista::seleccionar_nodo(int posicion){

    Nodo *seleccionado = this->primero;
    bool valido = 0;

    // Si la posición seleccionada es 1 y la lista no esta vacía, se marca como
    // válida la selección
    if (posicion == 1 && seleccionado != NULL){
        valido = 1;
    }
    else{

        int posicion_actual = 1;

        // En otro caso, se busca si la posición seleccionada es válida
        while(seleccionado != NULL){

            // En caso de serla, se marcará como válida
            if(posicion_actual == posicion){
                valido = 1;
                break;
            }

            posicion_actual++;
            seleccionado = seleccionado->sig;
        }
    }

    // Si la selección es válida, se muestra menú de opciones
    if(valido){

        string opcion;
        string aux;
        ListaIngredientes *ingredientes = seleccionado->ingredientes;

        while(1){

            system("clear");
            cout << "El postre seleccionado es: " << seleccionado->nombre << endl << endl;
            cout << "¿Qué desea hacer con el?" << endl;
            cout << "1.- Ver ingredientes" << endl;
            cout << "2.- Añadir ingredientes" << endl;
            cout << "3.- Eliminar ingredientes" << endl;
            cout << "4.- Volver al menu principal" << endl;
            getline(cin, opcion);

            // Opción 1 imprime la lista de ingredientes actual del postre
            if(opcion == "1"){

                ingredientes->imprimir_ingredientes();

                cout << "Presione enter para continuar..." << endl;
                getline(cin, aux);

            }

            // La opción 2 añade ingredientes a la lista de la misma forma que
            // se añaden postres, solo que esta lista no tiene orden
            else if(opcion == "2"){

                string ingrediente;

                cout << "Ingrese el nombre de su ingrediente: ";
                getline(cin, ingrediente);

                ingredientes->add_ingrediente(ingrediente);
                system("sleep 1");
            }

            // La opción 3 elimina ingredientes de la lista, de igual forma que
            // lo hace la lista de postres
            else if(opcion == "3"){

                string eliminar;

                ingredientes->imprimir_ingredientes();

                cout << "Ingrese el número del ingrediente que desea eliminar: ";
                getline(cin, eliminar);

                ingredientes->eliminar_ingrediente(stoi(eliminar));
                system("sleep 1");
            }

            // Con opción 4 simplemente se corta el bucle del menu de postre
            else if(opcion == "4"){
                break;
            }

            else{
                cout << "Opción no válida";
            }

            system("clear");

        }
    }

    else{
        cout << "El postre seleccionado no existe...";
        system("sleep 1");
    }
}
