#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{

    // Atributos que facilitan añadir elementos a la lista en forma ordenada
    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor por defecto
        Lista();
        // Función que permite la creación y enlace de nuevos nodos ordenados
        void crear_ordenados(string nombre);
        // Función que permite añadir nuevos nodos de forma desordenada
        void eliminar_nodo(int posicion);
        // Función que imprime nodos de la lista actual
        void imprimir();
        // Función que permite seleccionar un nodo para trabajar con sus ingredientes
        void seleccionar_nodo(int posicion);
};
#endif
