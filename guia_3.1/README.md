# Guia_3.1

Guia 3.1 - Listas enlazadas


## Ejercicio 1

>Escriba un programa que cree una lista desordenada de números y luego la divida en dos listas independientes ordenadas ascendentemente, una formada por los números positivos y otra por los números negativos.

En este programa inicialmente se pregunta al usuario cuantos número desea añadir a la lista inicial de números. A continuación el usuario debe ingresar cada uno de los números a la lista, estos pueden ser negativos o positivos. Los números quedan guardados en una lista inicial de forma desordenada, a continuación el programa se encarga de revisar cada uno de estos elementos y añadirlos a una lista con números positivos o negativos según corresponda, además, son ingresados de forma ordenada ascendentemente. Finalmente el programa imprime la lista inicial, la lista con números negativos y la lista con números positivos.

---

## Ejercicio 2

>Escriba un programa que cree una lista ordenada con nombres (string). Por cada ingreso, muestre el contenido de la lista.

Inicialmente el programa pregunta al usuario la cantidad de nombres que desea ingresar a la lista. A continuación el usuario debe ir ingresando cada uno de los nombres, por cada ingreso de nombre se imprime la lista actual. Los nombres se ingresan de forma ordenada alfabéticamente a la lista, para lograr esto, el programa revisa la primera letra de cada nombre y utilizando su valor en el código ASCII determina la posición correcta, en caso de empezar dos nombres con la misma letra, se revisan las siguientes letras hasta determinar su posición.

---

## Ejercicio 3

>Escriba un programa que permita mantener una lista ordenado alfabéticamente de postres. Cada postre,
debe mantener una lista de sus ingredientes (no necesariamente ordenada). [...].

Al inicio del programa, el usuario verá un menú con las siguientes opciones:
* Visualizar postres
* Añadir postre
* Eliminar postre
* Seleccionar postre
* Salir  

Visualizar postres imprimirá la lista de postres actual. Añadir postres pregunta al usuario por el nombre del postre para después añadirlo ordenado alfabéticamente de la misma forma que hace ejercicio 2. Eliminar postre imprime la lista y pregunta al usuario por el número del postre que desea eliminar, a continuación se elimina el postre que ocupa esa posición en la lista (si es que la posición seleccionada es válida).  

Seleccionar postre actúa igual que eliminar postre, pregunta la posición y si es que existe se despliega un menú con las siguientes opciones:
* Visualizar ingredientes
* Añadir ingrediente
* Eliminar ingrediente
* Volver al menú principal  

Cada opción hace lo mismo que su contraparte del menú principal, solo que ahora trabajan con la lista de ingredientes del postre seleccionado. La opción de volver al menú principal, así como la opción salir tienen nombres bastante descriptivos y es obvio lo que hacen, simplemente cortan un bucle que mantiene funcionando los menús.  
Se puede agregar ingredientes, después volver al menú principal y añadir otros postres, y cuando se vuelva a seleccionar el mismo postre, la lista de ingredientes permanece intacta.

---
