#include <iostream>
#include "Lista.h"

using namespace std;

// Constructor por defecto de la clase "Lista"
Lista::Lista() {}

// Función que permite la creación y enlazado de nodos
void Lista::crear_ordenados(string nombre){

    Nodo *temp = new Nodo;
    temp->nombre = nombre;

    // En caso de que no existan nodos en la lista, el primero y a la vez el
    // último va a ser el único nodo existente
    if (this->primero == NULL){
        this->primero = temp;
        this->ultimo = temp;
    }

    // En caso de que si exista al menos un elemento en la lista, se pasa a
    // comprobar la posición en que deberá ir el nodo
    else{

        // Se instancia una variable que permita recorrer la lista de nodos y
        // se guarda el nodo anterior, en caso de que empiecen dos palabras
        // con la misma letra pero una va antes que la otra
        Nodo *actual = this->primero;
        Nodo *anterior = NULL;

        // Mientras existan nodos en la lista, se comprobará la posición en
        // la que debe ir el nodo
        while(actual != NULL){

            // Si el valor en ascii de la primera letra es menor que la primera,
            // se posiciona al principio de la lista
            if ((int)temp->nombre[0] < (int)actual->nombre[0]){
                this->primero = temp;
                temp->sig = actual;
                break;
            }

            // Si el valor es mayor que el último de la lista, se coloca el
            // nodo al final de la lista y se enlaza como debe
            else if ((int)temp->nombre[0] > (int)this->ultimo->nombre[0]){
                this->ultimo->sig = temp;
                this->ultimo = temp;
                break;
            }

            // En caso de ser la misma letra, se pasa a revisar las siguientes
            else if ((int) temp->nombre[0] == (int) actual->nombre[0]){

                int i = 1;
                bool repetido = 1;

                // Si existe mas de una vez el mismo nombre, solo se compara el
                // último
                if (actual->sig != NULL){
                    if(actual->nombre == actual->sig->nombre){
                        actual = actual->sig;
                        continue;
                    }
                }

                // Se compara cada letra hasta ver cuál corresponde alfabeticamente
                while(temp->nombre[i] != '\0' || actual->nombre[i] != '\0'){

                    if((int)temp->nombre[i] > (int)actual->nombre[i]){
                        temp->sig = actual->sig;
                        actual->sig = temp;
                        repetido = 0;
                        break;
                    }

                    else if((int)temp->nombre[i] < (int)actual->nombre[i]){

                        if (anterior != NULL){
                            temp->sig = anterior->sig;
                            anterior->sig = temp;
                        }
                        else{
                            temp->sig = this->primero;
                            this->primero = temp;
                        }
                        repetido = 0;
                        break;
                    }
                    i++;
                }

                // Si todas las letras son iguales, se agrega el nodo al lado
                // de su repetido
                if(repetido){
                    temp->sig = actual->sig;
                    actual->sig = temp;
                }
                break;
            }

            // Si el valor es mayor o igual al elemento actual, o menor
            // o igual al elemento siguiente, se cambian los respectivos
            // punteros y se inserta el nodo
            else if ((int)temp->nombre[0] > (int)actual->nombre[0] && (int)temp->nombre[0] < (int)actual->sig->nombre[0]){
                temp->sig = actual->sig;
                actual->sig = temp;
                break;
            }

            // Si no se cumple ninguna de las anteriores, se pasa al siguiente
            // nodo de la lista para hacer comparaciones
            anterior = actual;
            actual = actual->sig;
        }
    }
}

// Función que imprime la lista de menor a mayor
void Lista::imprimir(){

    // Se instancia una variable auxiliar para recorrer la lista sin perder
    // los valores de la lista
    Nodo *aux = this->primero;

    // Siempre que exista un nodo (el valor del actual no sea NULL), se irá
    // imprimiendo sus valores
    while (aux != NULL){
        cout << "(" << aux->nombre << ")->";
        aux = aux->sig;
    }
    // Para representar el fin de la lista, se imprime la palabra FIN
    cout << "FIN" << endl << endl;
}
