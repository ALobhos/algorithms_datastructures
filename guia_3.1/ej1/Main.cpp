#include <iostream>
#include "Lista.h"

using namespace std;


// Función que reparte los valores negativos y positivos a sus respectivas listas
void repartir_lista(Lista &desordenados, Lista &positivos, Lista &negativos){

    Nodo *aux = desordenados.get_primero();

    // Se recorre la lista inicial y dependiendo de sus valores, se ingresan
    // a la lista positiva o negativa (de forma ordenada)
    while (aux != NULL){

        if (aux->numero < 0){
            negativos.crear_ordenados(aux->numero);
        }
        else{
            positivos.crear_ordenados(aux->numero);
        }

        aux = aux->sig;
    }

    cout << "Lista repartida con éxito!" << endl << endl;
}


// Función que permite llenar la lista inicial
void llenar_lista(Lista &desordenados){

    string cantidad;
    string numero;

    // Se pregunta cantidad de elementos que se desea añadir
    cout << "¿Cuántos números desea añadir a la lista inicial?" << endl;
    getline(cin, cantidad);

    // Con ayuda de un ciclo for, se añaden valores de forma desordenada
    for(int i=0; i<stoi(cantidad); i++){

        cout << endl << "Ingrese número para añadir a la lista (negativo o positivo)" << endl;
        getline(cin, numero);

        desordenados.crear_desordenados(stoi(numero));
    }

    cout << "Lista creada con exito!" << endl;
}


// Función principal
int main(){

    Lista desordenada;
    Lista positivos;
    Lista negativos;

    // Se llama a funciones que llenan y reparten los valores a ambas listas
    llenar_lista(desordenada);
    repartir_lista(desordenada, positivos, negativos);

    // Finalmente se imprimen: la lista inicial, la con números negativos y
    // la con números positivos
    cout << "Las listas son las siguientes:" << endl << endl;
    cout << "Lista original:" << endl;
    desordenada.imprimir();

    cout << "Lista de números negativos:" << endl;
    negativos.imprimir();

    cout << "Lista de números positivos:" << endl;
    positivos.imprimir();

    return 0;
}
