#include <iostream>

using namespace std;

#ifndef NODO_H
#define NODO_H

// Definición de la estructura nodo, la cual contiene un elemento de informacion
// (un entero), punteros hacia la izquierda y la derecha, estos apuntan al los siguientes nodos
// y el factor de equilibrio de cada nodo (también un entero)
typedef struct _Nodo{
    int numero;
    int equilibrio;
    struct _Nodo *izquierda = NULL;
    struct _Nodo *derecha = NULL;
}Nodo;
#endif
