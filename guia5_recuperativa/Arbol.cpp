#include <iostream>
#include "Arbol.h"

using namespace std;


// Constructor por defecto
Arbol::Arbol(){}


// Método para reestructuración izquierda (rama izquierda disminuyó tamaño)
void Arbol::reestructura_izq(Nodo *&nodo, bool &crece){

    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;

    // Si la altura del árbol cambió, se llevan a cabo comprobaciones
    if(crece){

        // Si la altura del nuevo árbol no viola las reglas de equilibrio,
        // solo se cambian valores del factor de equilibrio
        if(nodo->equilibrio == -1){
            nodo->equilibrio = 0;
        }
        else if(nodo->equilibrio == 0){
            nodo->equilibrio = 1;
            crece = 0;
        }

        // Si la nueva altura si viola las reglas, se debe reestructurar el
        // árbol, en ambos casos la columna derecha es demasiado larga
        else if(nodo->equilibrio == 1){
            nodo1 = nodo->derecha;

            // Si la columna avanza solo hacia la derecha, se realiza una
            // rotación DD
            if(nodo1->equilibrio >= 0){
                nodo->derecha = nodo1->izquierda;
                nodo1->izquierda = nodo;

                if(nodo1->equilibrio == 0){
                    nodo->equilibrio = 1;
                    nodo1->equilibrio = -1;
                    crece = 0;
                }
                else if(nodo1->equilibrio == 1){
                    nodo->equilibrio = 0;
                    nodo1->equilibrio = 0;
                }
                nodo = nodo1;
            }

            // En cambio si avanza hacia la derecha y despues hacia la izquierda
            // se realiza una rotación DI
            else{
                nodo2 = nodo1->izquierda;
                nodo->derecha = nodo2->izquierda;
                nodo2->izquierda = nodo;
                nodo1->izquierda = nodo2->derecha;
                nodo2->derecha = nodo1;

                if(nodo2->equilibrio == 1){
                    nodo->equilibrio = -1;
                }
                else{
                    nodo->equilibrio = 0;
                }

                if(nodo2->equilibrio == -1){
                    nodo1->equilibrio = 1;
                }
                else{
                    nodo1->equilibrio = 0;
                }

                nodo = nodo2;
                nodo2->equilibrio = 0;
            }
        }
    }
}



// Método para rotación derecha (si rama derecha disminuyó tamaño)
void Arbol::reestructura_der(Nodo *&nodo, bool &crece){

    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;

    // Si el árbol creció, se comprueba su balanceo
    if(crece){

        // En caso de cumplirse las reglas de equilibrio, solo se cambia
        // valores de equilibrio
        if(nodo->equilibrio == 1){
            nodo->equilibrio = 0;
        }
        else if(nodo->equilibrio == 0){
            nodo->equilibrio = -1;
            crece = 0;
        }

        // Si las reglas se violan, se debe reestructurar el árbol, la rama
        // izquierda es muy larga
        else if(nodo->equilibrio == -1){
            nodo1 = nodo->izquierda;


            // Si solo avanza hacia la izquierda, se realiza rotación II
            if(nodo1->equilibrio <= 0){
                nodo->izquierda = nodo1->derecha;
                nodo1->derecha = nodo;

                if(nodo1->equilibrio == 0){
                    nodo->equilibrio = -1;
                    nodo1->equilibrio = 1;
                    crece = 0;
                }
                else if(nodo1->equilibrio == -1){
                    nodo->equilibrio = 0;
                    nodo1->equilibrio = 0;
                }
                nodo = nodo1;
            }

            // Si avanza hacia la izquierda y después a la derecha, se realiza
            // rotación ID
            else{
                nodo2 = nodo1->derecha;
                nodo->izquierda = nodo2->derecha;
                nodo2->derecha = nodo;
                nodo1->derecha = nodo2->izquierda;
                nodo2->izquierda = nodo1;

                if(nodo2->equilibrio == -1){
                    nodo->equilibrio = 1;
                }
                else{
                    nodo->equilibrio = 0;
                }

                if(nodo2->equilibrio == 1){
                    nodo1->equilibrio = -1;
                }
                else{
                    nodo1->equilibrio = 0;
                }

                nodo = nodo2;
                nodo2->equilibrio = 0;
            }
        }
    }
}


// Método para ingresar valores al árbol (y mantenerlo balanceado)
void Arbol::insertar_numero(Nodo *&nodo, bool &crece, int numero){


    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;
    Nodo *otro = NULL;

    // Si el nodo actual no es nulo, se procede a comprobar
    if(nodo != NULL){

        // Si el número a ingresar es menor al número de la posición actual,
        // se revisa la siguiente posición por la izquierda (recursivamente)
        if(numero < nodo->numero){
            insertar_numero(nodo->izquierda, crece, numero);


            // Si el número se añadió por la izquierda, se revisa equilibrio
            if(crece){

                // En caso de no violarse ninguna regla, se mantiene el árbol
                // y se cambian valores de equilibrio
                if(nodo->equilibrio == 1){
                    nodo->equilibrio = 0;
                    crece = 0;
                }
                else if(nodo->equilibrio == 0){
                    nodo->equilibrio = -1;
                }

                // En caso de romperse el equilibrio se reestructura el árbol
                else if(nodo->equilibrio == -1){
                    nodo1 = nodo->izquierda;

                    // Si la rama izquierda es muy larga y solo avanza por la
                    // izquierda, se realiza rotación II
                    if(nodo1->equilibrio <= 0){
                        nodo->izquierda = nodo1->derecha;
                        nodo1->derecha = nodo;
                        nodo->equilibrio = 0;
                        nodo = nodo1;
                    }

                    // En cambio si avanza hacia la izquierda y despues a la
                    // derecha, se realiza una rotación ID
                    else{
                        nodo2 = nodo1->derecha;
                        nodo->izquierda = nodo2->derecha;
                        nodo2->derecha = nodo;
                        nodo1->derecha = nodo2->izquierda;
                        nodo2->izquierda = nodo1;

                        if(nodo2->equilibrio == -1){
                            nodo->equilibrio = 1;
                        }
                        else{
                            nodo->equilibrio = 0;
                        }

                        if(nodo2->equilibrio == 1){
                            nodo1->equilibrio = -1;
                        }
                        else{
                            nodo1->equilibrio = 0;
                        }

                        nodo = nodo2;
                    }
                    nodo->equilibrio = 0;
                    crece = 0;
                }
            }
        }
        else{

            // Si el número es mayor al de la posición actual, se continúa
            // revisando por la derecha (recursivamente)
            if(numero > nodo->numero){
                insertar_numero(nodo->derecha, crece, numero);

                // En caso de que el valor se inserte por la derecha, se revisa
                // si cumple con reglas de equilibrio
                if(crece){

                    // Si aún cumple las reglas, solo se cambian valores de
                    // equilibrio
                    if(nodo->equilibrio == -1){
                        nodo->equilibrio = 0;
                        crece = 0;
                    }
                    else if(nodo->equilibrio == 0){
                        nodo->equilibrio = 1;
                    }

                    // Si viola las reglas, se reestructura el árbol
                    else if(nodo->equilibrio == 1){
                        nodo1 = nodo->derecha;

                        // La rama derecha es muy larga, si solo avanza hacia la
                        // derecha, se realiza rotación DD
                        if(nodo1->equilibrio >= 0){
                            nodo->derecha = nodo1->izquierda;
                            nodo1->izquierda = nodo;
                            nodo->equilibrio = 0;
                            nodo = nodo1;
                        }

                        // Si en su lugar, va hacia la derecha y despues hacia
                        // la izquierda, se realiza rotación DI
                        else{
                            nodo2 = nodo1->izquierda;
                            nodo->derecha = nodo2->izquierda;
                            nodo2->izquierda = nodo;
                            nodo1->izquierda = nodo2->derecha;
                            nodo2->derecha = nodo1;

                            if(nodo2->equilibrio == 1){
                                nodo->equilibrio = -1;
                            }
                            else{
                                nodo->equilibrio = 0;
                            }

                            if(nodo2->equilibrio == -1){
                                nodo1->equilibrio = 1;
                            }
                            else{
                                nodo1->equilibrio = 0;
                            }
                            nodo = nodo2;
                        }
                        nodo->equilibrio = 0;
                        crece = 0;
                    }
                }
            }
            // Si nodo no es nulo, pero el valor no es mayor ni menor, significa
            // que el valor ya se encuentra en el árbol, por lo que no se añade
            else{
                cout << "El número ingresado ya se encuentra en el árbol" << endl;
            }
        }
    }
    // Si se llegó a un punto donde el nodo es nulo, se entiende que el número
    // no está en el árbol y que se debe agregar a la posición actual
    else{

        // Se crea el nodo con sus respectivos valores y se indica que el árbol
        // creció
        nodo = new Nodo();
        nodo->numero = numero;
        nodo->izquierda = NULL;
        nodo->derecha = NULL;
        nodo->equilibrio = 0;
        crece = 1;

        cout << "Número añadido con éxito al árbol!" << endl;
    }
}


// Método para eliminar un valor del árbol
void Arbol::eliminar_numero(Nodo *&nodo, bool &crece, int numero){

    Nodo *otro;
    Nodo *aux1;
    Nodo *aux2;
    bool aux;

    // Si el nodo actual no es nulo, se procede a comprobar valores
    if(nodo != NULL){

        // Si el número es menor al de la posición actual, se busca por la izquierda
        // recursivamente
        if(numero < nodo->numero){
            eliminar_numero(nodo->izquierda, crece, numero);
            this->reestructura_izq(nodo, crece);
        }

        // Si el número es mayor, se busca por la derecha de igual forma
        else if(numero > nodo->numero){
            eliminar_numero(nodo->derecha, crece, numero);
            this->reestructura_der(nodo, crece);
        }

        // Si el número es igual (ni menor ni mayor), se prodece a eliminar
        else{
            otro = nodo;
            crece = 1;

            // Si es una hoja o solo tiene un pariente, la Eliminación es sencilla
            if(otro->derecha == NULL){
                nodo = otro->izquierda;
            }

            else{
                if(otro->izquierda == NULL){
                    nodo = otro->derecha;
                }

                // En cambio si tiene dos parientes, la Eliminación es un poco
                // mas elaborada
                else{
                    aux1 = nodo->izquierda;
                    aux = 0;

                    // Se busca el número mas grande, menor que el que se quiere
                    // eliminar y por debajo de el
                    while(aux1->derecha != NULL){
                        aux2 = aux1;
                        aux1 = aux1->derecha;
                        aux = 1;
                    }

                    nodo->numero = aux1->numero;
                    otro = aux1;

                    if(aux){
                        aux2->derecha = aux1->izquierda;
                    }

                    else{
                        nodo->izquierda = aux1->izquierda;

                    }
                    // Se libera espacio en memoria y se comprueba equilibrio
                    // en la rama de la que se sacó el número
                    free(otro);
                    if(nodo->izquierda != NULL){
                        reestructura_der(nodo->izquierda, crece);
                    }
                }
            }
            cout << "Eliminación exitosa!" << endl;
        }
    }
    // Si se llega a algún nodo nulo, significa que el valor no está en el árbol
    else{
        cout << "La información no se encuentra en el árbol";
    }
}


// Método que comprueba si un valor está o no en el árbol
bool Arbol::en_arbol(int numero){

    Nodo *nodo = this->raiz;
    bool encontrado = 0;

    // Se recorre el árbol según las reglas de como esta hecho un árbol binario
    // hasta encontrar el número o llegar a un punto nulo (significa que el valor
    // no se encuentra en el árbol)
    while(nodo != NULL){

        if(nodo->numero == numero){
            encontrado = 1;
            break;
        }
        else if(numero < nodo->numero){
            nodo = nodo->izquierda;
        }
        else if(numero > nodo->numero){
            nodo = nodo->derecha;
        }
    }

    return encontrado;
}

// Método que retorna la raíz del árbol
Nodo *&Arbol::get_raiz(){
    return this->raiz;
}
