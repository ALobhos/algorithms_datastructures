#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

// Clase Árbol
class Arbol{

    private:
        // Puntero que representa la raíz del árbol
        Nodo *raiz = NULL;
        // Método para reestructuración izquierda del árbol
        void reestructura_izq(Nodo *&nodo, bool &crece);
        // Método para reestructuración derecha del árbol
        void reestructura_der(Nodo *&nodo, bool &crece);

    public:
        // Constructor por defecto
        Arbol();
        // Método para insertar valores al árbol balanceado
        void insertar_numero(Nodo *&nodo, bool &crece, int numero);
        // Método para eliminación de número en árbol balanceado
        void eliminar_numero(Nodo *&nodo, bool &crece, int numero);
        // Método que confirma si existe o no un número en el árbol
        bool en_arbol(int numero);
        // Método que retorna la raíz del árbol
        Nodo *&get_raiz();
};
#endif
