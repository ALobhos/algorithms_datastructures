#include <iostream>

using namespace std;


#ifndef HASH_H
#define HASH_H

class Hash_creator{

    private:
        string colisiones;
        int primo;
        void prueba_lineal(int *array, int n, int numero);
        // int prueba_cuadratica();
        // int doble_direccion();
        // int encadenamiento();

    public:
        Hash_creator(string metodo, int n);
        int generar_hash(int n, int numero, int *array, bool buscar);
        // int colision();
};
#endif
