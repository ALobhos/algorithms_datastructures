#include <iostream>
#include <stdlib.h>
#include "Hash_creator.h"


using namespace std;



void menu(string metodo){

    string n;
    string opcion;
    string numero;
    int clave;
    string aux;

    cout << "¿De que tamaño será el arreglo generado?" << endl;
    getline(cin, n);

    Hash_creator generador = Hash_creator(metodo, stoi(n));
    int array[stoi(n)] = {};

    system("clear");

    while(1){
        cout << "¿Que desea hacer?" << endl;
        cout << "1.- Ingresar un elemento al arreglo" << endl;
        cout << "2.- Buscar un elemento en el arreglo" << endl;
        cout << "3.- Salir" << endl;
        getline(cin, opcion);

        if (opcion == "1"){

            cout << "Ingrese número para añadir al arreglo: ";
            getline(cin, numero);

            clave = generador.generar_hash(stoi(n), stoi(numero), array, 0);

            if(clave != -1){
                array[clave] = stoi(numero);
            }
            else{
                cout << "El arreglo se encuentra actualmente lleno :C" << endl;
            }

            cout << "El arreglo actualmente es este:" << endl;
            cout << "Posicion | Valor" << endl;
            cout << "---------+-------" << endl;
            for(int i=0; i<stoi(n); i++){
                cout << i+1 << " | " << array[i] << endl;
            }

            cout << "Presione enter para continuar...." << endl;
            getline(cin, aux);
            system("clear");
        }

        else if(opcion == "2"){
            cout << "Ingrese número para buscar en el arreglo: ";
            getline(cin, numero);

            generador.generar_hash(stoi(n), stoi(numero), array, 1);
        }

        else if(opcion == "3"){
            exit(0);
        }

        else{
            cout << "La opción ingresada no es válida";
            system("sleep 2");
            system("clear");
        }
    }
}

int main(int argc, char **argv){

    string param;

    if (argc != 2){
        cout << "Debe ingresar 1 parámetro" << endl;
    }

    else{

        param = argv[1];

        if(param != "l" && param != "c" && param != "d" && param != "e"){
            cout << "Parámetro no válido, utilice cualquiera de las letras {'l', 'c', 'd', 'e'} como parámetro" << endl;
            exit(1);
        }

        menu(param);
    }



    return 0;
}
