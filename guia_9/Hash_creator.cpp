#include <iostream>
#include "Hash_creator.h"

using namespace std;


Hash_creator::Hash_creator(string metodo, int n){

    this->colisiones = metodo;

    bool primo = 1;

    while(n > 3){
        primo = 1;

        for(int i=2; i<=(n/2); i++){

            if(n % i == 0){
                primo = 0;
                break;
            }
        }

        if(primo){
            break;
        }
        n--;
    }
    this->primo = n;
}

void Hash_creator::prueba_lineal(int *array, int n, int numero){

    int d = (numero%this->primo);
    int d_aux;

    if(array[d] != 0 && array[d] == numero){
        cout << "El número buscado se encuentra en la posición " << d+1 << endl;
    }
    else{
        d_aux = d+1;

        while(d_aux < n && array[d_aux] != 0 && array[d_aux] != numero && d_aux != d){
            d_aux++;

            if(d_aux == n){
                d_aux = 0;
            }
        }
        cout << "COLISIÓN EN POSICIÓN " << d+1 << " DESPLAZAMIENTO FINAL A " << d_aux+1 << endl;
        if(array[d_aux] == 0 || d_aux == d){
            cout << "El elemento buscado no se encuentra en el arreglo" << endl;
        }
        else{
            cout << "El elemento buscado se encuentra en la posición " << d_aux+1 << endl;
        }

    }
}




int Hash_creator::generar_hash(int n, int numero, int *array, bool buscar){

    int clave = (numero%this->primo);
    int nueva;

    if(buscar){
        if(this->colisiones == "l"){
            this->prueba_lineal(array, n, numero);
        }
    }
    else{
        if(array[clave] != 0){

            if(this->colisiones == "l"){

                nueva = clave+1;
                while(nueva != clave){

                    if(array[nueva] == 0){
                        break;
                    }

                    nueva++;
                    cout << nueva << endl;

                    if(nueva == n){
                        nueva = 0;
                    }

                }
            }

            if(nueva == clave){
                cout << "COLISIÓN EN POSICIÓN " << clave+1 << " NO QUEDAN POSICIONES LIBRES" << endl;
                clave = -1;
            }
            else{
                cout << "COLISIÓN EN POSICIÓN " << clave+1 << " DESPLAZANDO A POSICIÓN " << nueva+1 << endl;
                clave = nueva;
            }
        }
    }


    return clave;
}
