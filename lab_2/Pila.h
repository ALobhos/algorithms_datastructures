#include <iostream>
#include <stack>
#include "Operador.h"

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{

    private:
        stack <int>  pila_numeros;
        Operador op;
        int largo;
        int tope;


    public:
        Pila(int n);
        void menu();
};
#endif
