#include <iostream>
#include <stack>

using namespace std;

#ifndef OPERADOR_H
#define OPERADOR_H

class Operador{

    public:
        Operador();
        bool pila_vacia(int tope);
        bool pila_llena(int tope, int largo);
        void revisar_pila(int tope, stack<int> pila);
        void add_elemento(int tope, int largo, stack<int> &pila);
        void quitar_elemento(stack<int> &pila, bool todos);
};
#endif
