#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{

    // Atributos que facilitan añadir elementos a la lista en forma ordenada
    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor por defecto
        Lista();
        // Función que permite la creación y enlace de nuevos nodos
        void crear(int numero);
        // Función que imprime nodos de la lista actual
        void imprimir();
};
#endif
