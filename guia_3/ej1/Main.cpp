#include <iostream>
#include "Lista.h"

using namespace std;

// Función principal
int main(){

    Lista lista;
    string cantidad;
    string numero;

    // Se pregunta al usuario cuantos números desea ingresar
    cout << "¿Cuántos números desea ingresar? " << endl;
    getline(cin, cantidad);

    // Se utiliza un ciclo for para ir añadiendo elementos a la vez que
    // se va imprimiendo la lista para su visualización

    for (int i=0; i<stoi(cantidad); i++){

        cout << "Ingrese número para añadir a la lista: ";
        getline(cin, numero);

        // Obtenido el número se llama a la función para añadir un nuevo nodo
        // y aquella que imprime la lista
        lista.crear(stoi(numero));
        cout << endl << "Lista actual:" << endl;
        lista.imprimir();

    }

    return 0;
}
