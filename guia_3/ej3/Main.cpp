#include <iostream>
#include "Lista.h"

using namespace std;

// Función que rellena los espacios entre números no correlativos entre sí
void rellenar_espacios(Lista &lista){

    // Se crea una instancia del primer elemento de la lista para poder recorrer
    // la lista mientras se llenan los espacios faltantes
    Nodo *aux = lista.get_primero();

    // Ahora se debe revisar que no quede nada frente al nodo que se esta revisando
    // con el objetivo de no generar una violación de segmento (además es innecesario
    // revisar mas allá del último elemento)
    while(aux->sig != NULL){

        // Si el número actual aumentado en 1 no es igual al siguiente al que
        // esta apunando, significa que no son correlativos, por lo que se procede
        // a llenar el espacio
        if((aux->numero + 1) != (aux->sig->numero)){

            // Con ayuda de un ciclo for, que tiene como inicio el elemento acutal
            // y como término el elemento al que apunta, se llena el espacio
            // faltante entre ambos elementos (no se llena nada si se añade dos
            // veces el mismo número a la lista)
            for(int i=aux->numero+1; i<aux->sig->numero; i++){
                lista.crear(i);
            }
        }

        // Se pasa al siguiente nodo
        aux = aux->sig;
    }

    // Mensaje para confirmar rellenado de la lista
    cout << "Se han llenado los espacios exitosamente!" << endl << endl;
}

// Función que permite al usuario llenar la lista
void llenar_lista(Lista &lista){

    string numero;
    string cantidad;

    // Se pregunta cuantos elementos se desea añadir inicialmente a la lista
    cout << "¿Cuantos números desea añadir a la lista?" << endl;
    getline(cin, cantidad);

    // Con la ayuda de un ciclo for se ingresan todos los valores deseados
    // a la lista, estos quedarán automáticamente ordenados gracias a la
    // función "crear" de la clase lista
    for(int i=0; i<stoi(cantidad); i++){

        cout << "Ingrese número para añadir a la lista: ";
        getline(cin, numero);

        lista.crear(stoi(numero));
    }

    // Mensaje para confirmar creación de lista
    cout << endl << "Lista creada exitosamente!" << endl << endl;
}

// Función principal
int main(){

    Lista lista;

    // Se llama a la función que permite la creación y llenado de la lista,
    // luego se imprime la lista recién creada
    llenar_lista(lista);
    cout << "La lista creada es la siguiente:" << endl;
    lista.imprimir();

    // Con la lista creada, se llama a función para rellenar espacios entre
    // números no correlativos, posteriormente se imprime la nueva lista
    rellenar_espacios(lista);
    cout << "La nueva lista con los espacios rellenados es la siguiente:" << endl;
    lista.imprimir();

    return 0;
}
