#include <iostream>

using namespace std;

// Definición de la estructura "Nodo", que contiene un puntero del tipo nodo
// que sirve de enlace, y un atributo para datos, en este caso un int
typedef struct _Nodo{
    int numero;
    struct _Nodo *sig = NULL;
} Nodo;
