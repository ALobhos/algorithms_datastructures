#include <iostream>
#include "Lista.h"

using namespace std;


// Función que junta ambas listas previamente creadas en una tercera lista
// que también estará ordenada
void juntar_listas(Lista *listas){

    // Se instancia un nodo auxiliar para recorrer ambas listas
    Nodo *aux = NULL;

    for(int i=0; i<2; i++){

        // Se imprime cada lista antes de ingresar valores en la 3ra lista
        cout << "Lista " << (i+1) << ":" << endl;
        listas[i].imprimir();

        // Se consigue el primer elemento de la lista actual para poder recorrerla
        // hasta el final
        aux = listas[i].get_primero();

        // Mientras queden nodos, se irán agregando sus valores a la 3ra lista
        while(aux != NULL){

            listas[2].crear(aux->numero);
            aux = aux->sig;
        }

    }

    // Una vez juntadas ambas listas en una tercera lista ordenada, se imprime
    // la lista recién creada
    cout << "Lista 3 creada al juntar listas 1 y 2:" << endl;
    listas[2].imprimir();
}

// Función que permite llenar ambas listas antes de juntarlas en una tercera
void llenar_listas(Lista *listas){

    string cantidad;
    string numero;

    // Se crea un ciclo que permite llenar ambas listas sin repetir tanto código
    for(int i=0; i<2; i++){

        // Se pregunta al usuario cuantos elementos tendrá cada lista
        cout << "¿Cuántos números desea ingresar a la lista " << (i+1) << "?" << endl;
        getline(cin, cantidad);

        // Se llena cada lista de forma ordenada según la cantidad de elementos
        // que quiere ingresar el usuario
        for(int j=0; j<stoi(cantidad); j++){

            cout << endl << "¿Que número desea ingresar?" << endl;
            getline(cin, numero);

            // Se llama a la función que añade elementos y los ordena de
            // forma automática a medida que se ingresan (ver implementación
            // de clase para detalles)
            listas[i].crear(stoi(numero));
        }
        // Mensaje para indicar creación exitosa
        cout << endl << "Lista " << (i+1) << " creada con éxito!" << endl << endl;
    }

}


// Función principal del programa
int main(){

    // Se crea un arreglo que contiene las 3 listas, por temas de orden y
    // eficiencia meramente
    Lista listas[3];

    // Primero se juntan crean ambas listas y despues se juntan
    llenar_listas(listas);
    juntar_listas(listas);

    return 0;
}
