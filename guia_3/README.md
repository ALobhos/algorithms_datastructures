# Guia_3

```bash
pip install foobar
```
Guia Nro. 3 - Listas enlazadas

## Ejercicio 1

>El propósito de este ejercicio es la creación de una lista enlazada que ya vaya ordenada a medida que se añadan elementos a ella, en este caso, números enteros.  

El programa primero pide al usuario saber cuantos elementos desea añadir a la lista, a continuación se preguntará cada uno de estos elementos. A medida que se van ingresando, los elementos quedarán automáticamente en su lugar correspondiente y se imprimirá la lista después de cada inserción.



## Ejercicio 2

>En este ejercicio se deben crear dos listas enlazadas simples (que el usuario debe llenar) y después se debe crear una tercera lista que consiste en juntar las dos listas previamente creadas en una sola lista ordenada.  

Primero se pregunta al usuario cuantos elementos desea ingresar a la lista 1, y se llena de la misma forma que se hacía en ejercicio 1, lo mismo ocurre con la segunda lista. A continuación el programa recorre ambas listas (una por una), añadiendo los valores de cada lista en una tercera que a medida que se ingresan, quedan automáticamente en la posición que deben ocupar (quedan ordenados de menor a mayor). Finalmente se imprimen las 3 listas creadas.

---

## Ejercicio 3

>Este ejercicio consiste en la creación de una lista enlazada simple en la cual pueden existir valores no correlativos entre sí. A continuación se debe procesar la lista para rellenar los espacios entre aquellos números que no sean correlativos, generando una lista que finalmente sera una progresión numérica con diferencia 1.

Primero se preguntará al usuario cuántos números desea ingresar a la lista para posteriormente preguntar por cada uno de estos números. Una vez creada la lista, se llamará internamente a una función que rellenará los espacios entre aquellos números que no sean correlativos entre sí con los respectivos valores que deban ir entre ambos. Finalmente se imprimen ambas listas para si visualización y estudio.

---
