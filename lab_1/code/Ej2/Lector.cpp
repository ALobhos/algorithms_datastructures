#include <iostream>
#include <ctype.h>
#include "Lector.h"

using namespace std;

// Constructor
Lector::Lector(){}


// Se utilizan dos funciones casi iguales con el objetivo de no considerar
// espacios en blanco en caso de ingresarse, si no existieran espacios en
// blanco se podría simplemente contar mayúsculas y la diferencia son
// minúsculas
int Lector::leer_mayusculas(string frase){

    this->mayusculas = 0;

    // Si la letra es mayúscula, se suma uno al contador que se retornará
    for(int i=0; i<frase.size(); i++){
        if(isupper(frase[i])){
            this->mayusculas++;
        }
    }

    return this->mayusculas;
}


int Lector::leer_minusculas(string frase){

    this->minusculas = 0;

    // Si la letra es minúscula, se suma uno al contador que se retornará
    for(int i=0; i<frase.size(); i++){
        if(islower(frase[i])){
            this->minusculas++;
        }
    }

    return this->minusculas;
}
