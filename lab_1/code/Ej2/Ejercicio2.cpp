#include <iostream>
#include "Lector.h"

using namespace std;


void revisar_frases(string *frases, int n){

    // Se crea el objeto reader para leer la letras
    Lector reader = Lector();

    // Se recorre cada frase y se cuentan sus mayúsculas y minúsculas
    for(int i=0; i<n; i++){
        cout << "La frase " << frases[i] << " tiene: " << endl;
        cout << reader.leer_mayusculas(frases[i]) << " minúsculas" << endl;
        cout << reader.leer_minusculas(frases[i]) << " mayúsculas" << endl << endl;
    }
}

int main(){

    string n;

    cout << "¿Cuántas palabras desea ingresar?" << endl;
    getline(cin, n);

    // Se crea arreglo de strings (cadenas de caracteres)
    string frases[stoi(n)];

    // Se utiliza un ciclo para añadir frases al arreglo
    for(int i=0; i<stoi(n); i++){
        cout << "Ingrese la frase " << (i+1) << ": ";
        getline(cin, frases[i]);
    }

    // Se llama a función para revisar las letras de cada frase
    revisar_frases(frases, stoi(n));

    return 0;
}
