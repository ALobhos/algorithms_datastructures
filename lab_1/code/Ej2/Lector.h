#include <iostream>

using namespace std;

#ifndef LECTOR_H
#define LECTOR_H

class Lector{
    private:
        int mayusculas;
        int minusculas;
    public:
        Lector();
        int leer_minusculas(string frase);
        int leer_mayusculas(string frase);
};
#endif
