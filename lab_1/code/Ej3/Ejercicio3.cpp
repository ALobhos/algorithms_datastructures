#include <iostream>
#include <stdlib.h>
#include "Cliente.h"

using namespace std;


void imprimir_datos_clientes(Cliente *clientes, int n){

    cout << "Los clientes ingresados son los siguientes: " << endl;

    // Se recorre el arreglo de clientes y se obtienen los datos de cada uno
    for(int i=0; i<n; i++){

        cout << "Cliente " << (i+1) << ":" << endl;
        cout << "Nombre: " << clientes[i].get_nombre() << endl;
        cout << "Teléfono: " << clientes[i].get_telefono() << endl;
        cout << "Saldo: $" << clientes[i].get_saldo() << endl;

        if(clientes[i].get_moroso()){
            cout << "Este cliente es un moroso >:C" << endl << endl;
        }
        else{
            cout << "Este cliente NO es un moroso :D" << endl << endl;
        }
    }
}


void obtener_datos_clientes(Cliente *clientes, int n){

    string nombre;
    string numero;
    string saldo;
    string moroso;

    cout << "Por favor, ingrese los datos de sus clientes:" << endl;

    // Cada cliente tiene un nombre, un número telefónico, un saldo y un
    // booleano que representa si es o no un moroso
    for(int i=0; i<n; i++){
        cout << "CLIENTE " << (i+1) << ":" << endl;
        cout << "Nombre: ";
        getline(cin, nombre);

        cout << "Teléfono: ";
        getline(cin, numero);

        cout << "Saldo: ";
        getline(cin, saldo);

        cout << "¿Este cliente es moroso? (s/n): ";
        getline(cin, moroso);

        while(moroso != "s" && moroso != "n"){
            cout << "Opción no válida, por favor utilice s/n";
            getline(cin, moroso);
        }

        // Se añaden los datos obtenidos previamente y se pasa a captar
        // los datos del siguiente cliente
        clientes[i].set_nombre(nombre);
        clientes[i].set_telefono(numero);
        clientes[i].set_saldo(stoi(saldo));

        if(moroso == "s"){
            clientes[i].set_moroso(1);
        }
        else{
            clientes[i].set_moroso(0);
        }
    }
}


int main(){

    string n;
    cout << "¿Cuántos clientes desea ingresar?" << endl;
    getline(cin, n);

    // Se crea arreglo de objetos del tipo "Cliente"
    Cliente clientes[stoi(n)];

    // Primero se obtienen datos de cada cliente, luego se imprimen los
    // datos obtenidos y guardados
    obtener_datos_clientes(clientes, stoi(n));
    system("clear");
    imprimir_datos_clientes(clientes, stoi(n));

    return 0;
}
