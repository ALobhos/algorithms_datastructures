#include <iostream>
#include "Cuadrados.h"

using namespace std;


int main(){

    string n;
    Cuadrados cuadrado = Cuadrados();

    cout << "¿Cuántos números desea ingresar?" << endl;
    getline(cin, n);

    int arreglo[stoi(n)];

    // Se añaden números a cada posición del arreglo previamente definido
    for (int i=0; i<stoi(n); i++){

        cout << "Ingrese número para ingresar en la posición " << (i+1) << " del arreglo: ";
        cin >> arreglo[i];
    }

    // Se envía arreglo a función que retornará la suma de los valores al
    // cuadrado
    cout << "La suma del cuadrado de todos sus números es: " << cuadrado.sumar_cuadrados(arreglo, stoi(n)) << endl;


    return 0;
}
