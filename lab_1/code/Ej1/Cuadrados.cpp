#include <iostream>
#include "Cuadrados.h"

using namespace std;

// Constructor por defecto
Cuadrados::Cuadrados(){}


int Cuadrados::sumar_cuadrados(int *arreglo, int n){

    int suma = 0;

    // Se crea una variable suma a la que se le va sumando el cuadrado de cada
    // número ingresado. Se retorna suma total
    for(int i=0; i<n; i++){
        suma += arreglo[i] * arreglo[i];
    }

    return suma;
}
