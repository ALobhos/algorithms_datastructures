#include <iostream>

using namespace std;

#ifndef CUADRADOS_H
#define CUADRADOS_H

class Cuadrados{

    public:
        Cuadrados();
        int sumar_cuadrados(int *arreglo, int n);
};
#endif
