#include <iostream>
#include <stack>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;


Pila::Pila(int limite){

    this->limite = limite;
    this->tope = 0;
    this->contenedores = new Contenedor[limite];
}


bool Pila::pila_vacia(){

    bool vacia = 0;

    if(this->tope == 0){
        vacia =1;
    }

    return vacia;
}


bool Pila::pila_llena(){

    bool llena = 0;

    if(this->tope == this->limite){
        llena = 1;
    }

    return llena;
}


void Pila::push(Contenedor contenedor){

    this->contenedores.push(contenedor);
    this->tope++;
}


void Pila::top(){

    this->contenedores[this->tope] = NULL;
    this->tope--;
}


int Pila::buscar_contenedor(string empresa, int numero){

    int posicion = 0;

    for(int i=0; i<this->tope; i++){

        if(this->contenedores[i].get_empresa() == empresa && this->contenedores[i].get_numero() == numero){
            posicion = i+1;
            break;
        }
    }

    return posicion;
}
