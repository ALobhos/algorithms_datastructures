#include <iostream>

using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H


class Contenedor{

    private:
        string empresa;
        int numero;

    public:
        Contenedor();
        Contenedor(string empresa, int numero);
        void set_empresa(string empresa);
        void set_numero(int numero);
        string get_empresa();
        int get_numero();
};
#endif
