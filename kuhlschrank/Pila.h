#include <iostream>
#include <stack>
#include "Contenedor.h"

using namespace std;

#ifndef PILA_H
#define PILA_H


class Pila{

    private:
        int limite;
        int tope;
        Contenedor contenedores;

    public:
        Pila(int limite);
        bool pila_llena();
        bool pila_vacia();
        void push(Contenedor contenedor);
        void top();
        int buscar_contenedor(string empresa, int numero);
        Contenedor get_contenedores();
};
#endif
