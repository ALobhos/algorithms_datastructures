#include <iostream>
#include <stdlib.h>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;


int menu(){

    string opcion;

    cout << "*** Generador de arboles AVL ***" << endl << endl;
    cout << "1.- Ingresar un número al árbol" << endl;
    cout << "2.- Borrar un número del árbol" << endl;
    cout << "3.- Generar grafo correspondiente al árbol" << endl;
    cout << "4.- Salir del programa" << endl << endl;
    cout << "Por favor seleccione una opción: ";
    getline(cin, opcion);

    return stoi(opcion);
}

int main(){

    int opcion;
    string numero;
    Arbol arbol_avl;
    Grafo generador;

    while(1){

        opcion = menu();

        if (opcion == 1){
            cout << "Ingrese numero para añadir al arbol: ";
            getline(cin, numero);

            arbol_avl.add_elemento(arbol_avl.get_raiz(), 0, stoi(numero));
            system("sleep 2");
            system("clear");
        }

        else if(opcion == 3){
            generador.generar_grafo(arbol_avl.get_raiz());
        }

        else if(opcion == 4){
            break;
        }
        else{
            cout << "Función actualmente en construcción" << endl;
            system("sleep 2");
            system("clear");
        }
    }


    return 0;
}
