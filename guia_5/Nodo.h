#include <iostream>

using namespace std;

#ifndef NODO_H
#define NODO_H

// Definición de la estructura nodo, la cual contiene un elemento de informacion
// (un entero), su factor de equilibrio, y punteros hacia la izquierda y la derecha,
// estos apuntan al los siguientes nodos
typedef struct _Nodo{
    int numero;
    int equilibrio;
    struct _Nodo *izquierda = NULL;
    struct _Nodo *derecha = NULL;
}Nodo;
#endif
