#include <iostream>
#include "Profesor.h"

using namespace std;

#ifndef UNIVERSIDAD_H
#define UNIVERSIDAD_H

class Universidad{

    private:
        Profesor *profesores = NULL;
        int promedio;

    public:
        Universidad();
        void menu(int n);
        void add_profesores(int n);
        void menor_edad(int n);
        void mayor_edad(int n);
        int promedio_edad(int n);
        int mayor_promedio(int n);
        int menor_promedio(int n);
};
#endif
