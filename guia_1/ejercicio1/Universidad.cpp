#include <iostream>
#include "Universidad.h"
#include "Profesor.h"

using namespace std;

Universidad::Universidad(){
    this->promedio = 0;
}

void Universidad::menu(int n){

    string opcion;
    string aux;

    while(1){
        system("clear");
        cout << "¿Que desea hacer?" << endl;
        cout << "1.- Revisar promedio de edad" << endl;
        cout << "2.- Profesor mas viejo" << endl;
        cout << "3.- Profesor mas joven" << endl;
        cout << "4.- Profesoras sobre el promedio de edad" << endl;
        cout << "5.- Profesores bajo el promedio de edad" << endl;
        cout << "6.- Salir" << endl;

        getline(cin, opcion);

        if (opcion == "1"){
            cout << "El promedio de edad de los profesores es: "
                 << promedio_edad(n) << " (enter para continuar)" << endl;

            getline(cin, aux);
        }

        else if (opcion == "2"){
            mayor_edad(n);
            getline(cin, aux);
        }

        else if (opcion == "3"){
            menor_edad(n);
            getline(cin, aux);
        }

        else if (opcion == "4"){
            cout << "Hay un total de " << mayor_promedio(n)
                 << " profesoras con edad mayor al promedio" << endl;
            getline(cin, aux);
        }

        else if (opcion == "5"){
            cout << "Hay un total de " << menor_promedio(n)
                 << " profesores con edad mayor al promedio" << endl;
            getline(cin, aux);
        }

        else if (opcion == "6"){
            break;
        }

        else {
            cout << "Opción no válida";
            system("sleep 1");
        }

    }

}

void Universidad::add_profesores(int n){

    this->profesores = new Profesor[n];
    string nombre;
    string sexo;
    string edad;

    for(int i=0; i<n; i++){
        cout << "¿Cual es el nombre del profesor " << (i+1) << "?" << endl;
        getline(cin, nombre);

        cout << "¿Cual es el sexo de este profesor (m/f)?" << endl;
        getline(cin, sexo);

        while(sexo != "m" && sexo != "f"){
            cout << "Opción no válida >:c" << endl;
            getline(cin, sexo);
        }

        cout << "¿Qué edad (en años) tiene el/la docente?" << endl;
        getline(cin, edad);

        this->profesores[i].set_nombre(nombre);
        this->profesores[i].set_sexo(sexo);
        this->profesores[i].set_edad(stoi(edad));
    }

    menu(n);
}

void Universidad::menor_edad(int n){

    int menor = 100;
    int menor_i;

    for (int i=0; i<n; i++){
        if (this->profesores[i].get_edad() < menor){
            menor = this->profesores[i].get_edad();
            menor_i = i;
        }
    }

    cout << "El/la profesor/a de menor edad se llama: "
         << this->profesores[menor_i].get_nombre() << endl;

    cout << "Edad: " << this->profesores[menor_i].get_edad() << endl;
}


void Universidad::mayor_edad(int n){

    int mayor = 0;
    int mayor_i;

    for (int i=0; i<n; i++){

        if (this->profesores[i].get_edad() > mayor){
            mayor = this->profesores[i].get_edad();
            mayor_i = i;
        }
    }

    cout << "El/la profesor/a de mayor edad se llama: "
         << this->profesores[mayor_i].get_nombre() << endl;

    cout << "Edad: " << this->profesores[mayor_i].get_edad() << endl;
}


int Universidad::promedio_edad(int n){

    int suma = 0;

    for (int i=0; i<n; i++){
        suma += this->profesores[i].get_edad();
    }

    return suma/n;
}

int Universidad::mayor_promedio(int n){

    if (this->promedio == 0){
        this->promedio = promedio_edad(n);
    }

    int mayor_al_promedio = 0;

    for (int i=0; i<n; i++){

        if (this->profesores[i].get_sexo() == "femenino"){
            if(this->profesores[i].get_edad() > this->promedio){
                mayor_al_promedio++;
            }
        }
    }

    return mayor_al_promedio;
}

int Universidad::menor_promedio(int n){

    if (this->promedio == 0){
        this->promedio = promedio_edad(n);
    }

    int menor_al_promedio = 0;

    for (int i=0; i<n; i++){

        if (this->profesores[i].get_sexo() == "masculino"){
            if(this->profesores[i].get_edad() < this->promedio){
                menor_al_promedio++;
            }
        }
    }

    return menor_al_promedio;
}
